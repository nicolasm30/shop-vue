import {
  defineStore
} from 'pinia'
import Data from '../assets/data/products.json'

export const useProductsStore = defineStore("products", {
  state: () => ({
    products: [],
  }),
  getters: {
    getProducts(state) {
      return state.products
    }
  },
  actions: {
    fetchProducts() {
      Data.products.map((dt, index) => {
        dt.id = index
      })
      this.products = Data.products
    }
  }
})
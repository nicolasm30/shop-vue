import {
    defineStore
  } from 'pinia'
  
  export const useCartStore = defineStore("cart", {
    state: () => ({
        myShop: [],
        total : 0,
        isProducts: false,
        message: '',
        isValid: false
    }),
    getters: {},
    actions: {
        addtoCart({id,name, unit_price, stock}, qty) {
            if (qty === null || qty.length == 0) {
              this.isValid = true
              this.message = 'You have not entered any value in the amount'
            } else {
              if (qty <= stock) {
                this.isValid = false
                this.isProducts = true
                this.myShop.push({id, name , unit_price,qty,totalProduct: qty * unit_price})
                this.total = this.myShop.reduce((accum,item) => accum + item.totalProduct, 0)
              } else {
                this.isValid = true
                this.message = `We don't have the number of ${qty} for the product ${name}`

              }
            }
        },
        generateOrder(){
            let order = {}
            order = {
                "shops" : this.myShop,
                "total" : this.total
            }
            let procesOrder = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(order));
            let download = document.createElement('a')
            download.href = procesOrder;
            download.setAttribute('download', "order.json")
            download.click()
        }
    }
  })